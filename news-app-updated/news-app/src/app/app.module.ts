import { AdminHeaderComponent } from './core/app-layout/admin-header/admin-header.component';
import { AdminFooterComponent } from './core/app-layout/admin-footer/admin-footer.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { LayoutComponent } from './core/layout/layout.component';
import { AdminLayoutComponent } from './core/app-layout/admin-layout/admin-layout.component';
import { SiteLayoutComponent } from './core/app-layout/site-layout/site-layout.component';
import { SiteHeaderComponent } from './core/app-layout/site-header/site-header.component';
import { SiteFooterComponent } from './core/app-layout/site-footer/site-footer.component';
import { AdminSidebarComponent } from './core/app-layout/admin-sidebar/admin-sidebar.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    SiteLayoutComponent,
    AdminLayoutComponent,
    SiteHeaderComponent,
    SiteFooterComponent,
    AdminFooterComponent,
    AdminHeaderComponent,
    AdminSidebarComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
