import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../../../../core/services/api.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
  editProduct: FormGroup;
  submitted = false;
  pro_id = '';
  name = '';
  description = '';
  pro_img = '';
  url = '';
  is_img_change = false;
  event: EventEmitter<any> = new EventEmitter();
  img_invalid = false;
  data = {
    name: '',
    id: '',
    image: '',
    description: ''
  };


  @Input() product_id;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private formBuilder: FormBuilder,
    private bsModalRef: BsModalRef,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.getProduct(this.product_id);
    this.editProduct = this.formBuilder.group({
      'name': [null, Validators.required],
      'description': [null, Validators.required],
    });
  }

  get validate() {
    return this.editProduct.controls;
  }

  //Get product by id from product list
  getProduct(id) {
    this.api.getById('product', id).subscribe(response => {
      this.pro_id = id;
      this.editProduct.setValue({
        name: response.name,
        description: response.description
      });
      this.pro_img = response.image;
    });
  }
  // Get Img URl/name upload from local
  onFileChanged(event) {
    this.is_img_change = true;
    this.img_invalid = false;
    this.url = event.target.files[0].name
    this.api.get('product')
    .subscribe(response => {
      response.forEach(element => {
        if (element.image === this.url) {
          this.img_invalid = true;
        }
      });
    }, err => console.log(err)
    );
  }

  closeModal() {
    this.bsModalRef.hide();
  }

  //Update product information in db.json file by id
  updateProduct(form: NgForm) {
    this.submitted = true;
    this.data.name = form.name;
    // this.data.description = form.description;
    this.data.id = this.pro_id;
    this.data.image = this.is_img_change ? this.url : this.pro_img;   
    if (this.editProduct.valid && !this.img_invalid) {
      this.api.update('product/' + this.pro_id, this.data)
        .subscribe(res => {
          this.closeModal();
          this.event.emit(true);
          this.is_img_change = false;
        }, (err) => {
          console.log(err);
        }
        );
    }else{
      console.log('try again!')
    }
  }
}
