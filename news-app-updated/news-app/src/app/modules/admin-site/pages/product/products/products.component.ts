import { ProductEditComponent } from './../product-edit/product-edit.component';
import { ApiService } from '../../../../../core/services/api.service';
import { Component, OnInit } from '@angular/core';
import { Product } from 'src/json/product';
import { CommonModule } from "@angular/common";
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ModalModule, BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[] = [];
  bsModalRef: BsModalRef;
  selectedProduct: {};
  pro_name: String = '';
  pro_desc: String = '';
  pro_id: string = '';
  url: string = '';
  constructor(
    private api: ApiService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.getProducts();
  }

  //Get all products
  getProducts() {
    this.api.get('product')
      .subscribe(response => {
        this.products = response;
        console.log(this.products);
      }, err => console.log(err)
      );
  }

  // Delete product by id in db.json file
  deleteItem(id, index) {
    this.api.delete('product', id)
      .subscribe(res => {
        this.products.splice(index, 1);
        setTimeout(() => {
          alert("Deleted!");
        }, 1000);
      }, (err) => console.log(err)
      );
  }

  // Open modal using ngx-bootstrap modal pass component
  openModal(id) {
    this.bsModalRef = this.modalService.show(ProductEditComponent, {
      backdrop: 'static',
      keyboard: false,
      animated: true,
      ignoreBackdropClick: true,
      initialState: {
        product_id: id
      }
    });

    //Refresh data after submit update
    this.bsModalRef.content.event.subscribe(result => {
      if (result) {
        this.getProducts();
      }
    });
  }
}

