import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  product = {
    name: '',
    description: '',
    image: ''
  };

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService) { }

  ngOnInit() {
    this.getProductById(this.route.snapshot.params['id']);
  }

  getProductById(id) {
    this.api.getById('product', id)
      .subscribe(response => {
        this.product.name = response.name;
        this.product.description = response.description;
        this.product.image = response.image;
      });
  }

}
