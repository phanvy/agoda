import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../../core/services/api.service';
import { Product } from '../../../../../../json/product';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  addProduct: FormGroup;
  submitted = false;
  name: string = '';
  description: string = '';
  data = {
    id: 0,
    name: '',
    description: '',
    image: ''
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.addProduct = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  get validate() { 
    return this.addProduct.controls; 
  }

  //Insert new product in db.json file
  newProduct(form) {
    this.submitted = true;
    if(this.addProduct.valid){
      this.data.id = new Date().getTime()
      this.data.name = form.name;
      this.data.description = form.description;
      this.api.post('product', this.data)
        .subscribe(response => {
          alert('Add new product successfully!')
          this.addProduct.reset();
          this.submitted = false;
        }, (err) => {
          console.log(err);
        });
    }
  }
}
