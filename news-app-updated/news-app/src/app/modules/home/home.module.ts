import { HomeRoutingModule } from './home-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './pages/home/home.component';
import { EditorPickComponent } from './components/editor-pick/editor-pick.component';
import { SlideshowComponent } from './components/slideshow/slideshow.component';
import { TrendComponent } from './components/trend/trend.component';
import { PoliticsComponent } from './components/politics/politics.component';
import { BusinessComponent } from './components/business/business.component';
import { RecentNewsComponent } from './components/recent-news/recent-news.component';
import { PopularPostComponent } from './components/popular-post/popular-post.component';

@NgModule({
  declarations: [
    HomeComponent,
    EditorPickComponent,
    SlideshowComponent,
    TrendComponent,
    PoliticsComponent,
    BusinessComponent,
    RecentNewsComponent,
    PopularPostComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule
  ],
  exports: []
})
export class HomeModule { }
