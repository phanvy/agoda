import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from '../shared/components/loader/loader.component';
import { ButtonsComponent } from '../shared/components/buttons/buttons.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    LoaderComponent,
    ButtonsComponent
  ],
  imports: [
    CommonModule,
    // RouterModule
  ],
  exports: [
    // RouterModule
  ]
})
export class SharedModule { }
