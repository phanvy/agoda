import { AdminLayoutComponent } from './core/app-layout/admin-layout/admin-layout.component';
import { AdminSiteModule } from './modules/admin-site/admin-site.module';
import { DashboardComponent } from './modules/admin-site/pages/dashboard/dashboard.component';
import { HomeComponent } from './modules/home/pages/home/home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SiteLayoutComponent } from './core/app-layout/site-layout/site-layout.component';
const routes: Routes = [
  // Site routes goes here
  {
    path: '',
    component: SiteLayoutComponent,
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)
  },
  // Admin routes goes here here
  {
      path: 'admin',
      component: AdminLayoutComponent,
      loadChildren: () => import('./modules/admin-site/admin-site.module').then(m => m.AdminSiteModule)
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
